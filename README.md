# distributed-bank-accounts

This repository contains variations of a distributed bank-account class library

* Variation 1 - local bank accounts, with features for 
  * creating new bank accounts, either checking or saving
  * closing a bank account that has a zero balance
  * deposit money
  * withdrawing money, if not beyond overdraft limit
	* Assess fee in overdraft
  * keep track of all bank accounts	
  * computing interest rate
  * computing average end-of-day balance
  * computing average deposits / day
  * computing average withdrawal / day
  * computing average of days in overdraft
 
* Variation 2 - distributed bank accounts
  